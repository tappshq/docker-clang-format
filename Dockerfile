FROM debian:testing-slim

RUN apt-get update \
    && apt-get install -y \
        clang-format \
        git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
